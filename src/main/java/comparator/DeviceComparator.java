package comparator;

import java.util.Comparator;
import model.Device;

public class DeviceComparator implements Comparator<Device> {

  @Override
  public int compare(Device o1, Device o2) {
    return Double.compare(o1.getComplection().getInterPrice(), o2.getComplection().getInterPrice());
  }
}
