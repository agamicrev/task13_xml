package parser.stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import model.Complection;
import model.Device;
import model.Type;

public class StAXReader {

  public static List<Device> parseDevices(File xml, File xsd) {
    List<Device> deviceList = new ArrayList<>();
    Device device = null;
    Complection complection = null;
    List<Type> types = null;

    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "device":
              device = new Device();

              Attribute idAttr = startElement.getAttributeByName(new QName("deviceID"));
              if (idAttr != null) {
                device.setDeviceID(Integer.parseInt(idAttr.getValue()));
              }
              break;
            case "name":
              xmlEvent = xmlEventReader.nextEvent();
              assert device != null;
              device.setName(xmlEvent.asCharacters().getData());
              break;
            case "price":
              xmlEvent = xmlEventReader.nextEvent();
              assert device != null;
              device.setPrice(xmlEvent.asCharacters().getData());
              break;
            case "critical":
              xmlEvent = xmlEventReader.nextEvent();
              assert device != null;
              device.setСritical(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            case "origin":
              xmlEvent = xmlEventReader.nextEvent();
              assert device != null;
              device.setOrigin(xmlEvent.asCharacters().getData());
              break;
            case "types":
              xmlEvent = xmlEventReader.nextEvent();
              types = new ArrayList<>();
              break;
            case "type":
              xmlEvent = xmlEventReader.nextEvent();
              assert types != null;
              types.add(new Type(xmlEvent.asCharacters().getData()));
              break;
            case "complection":
              xmlEvent = xmlEventReader.nextEvent();
              complection = new Complection();
              break;
            case "interPrice":
              xmlEvent = xmlEventReader.nextEvent();
              assert complection != null;
              complection.setInterPrice(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            case "multimedia":
              xmlEvent = xmlEventReader.nextEvent();
              assert complection != null;
              complection.setMultimedia(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              assert device != null;
              device.setComplection(complection);
              device.setTypes(types);
              break;
          }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("device")) {
            deviceList.add(device);
          }
        }
      }

    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    return deviceList;
  }
}
