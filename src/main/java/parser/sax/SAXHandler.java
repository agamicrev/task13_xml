package parser.sax;


import java.util.ArrayList;
import java.util.List;
import model.Complection;
import model.Device;
import model.Type;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler {

  private List<Device> deviceList = new ArrayList<>();
  private Device device = null;
  private List<Type> types = null;
  private Complection complection = null;


  private boolean bName = false;
  private boolean bPrice = false;
  private boolean bСritical = false;
  private boolean bOrig = false;
  private boolean bTypes = false;
  private boolean bType = false;
  private boolean bComplection = false;
  private boolean bInp = false;
  private boolean bMult = false;

  public List<Device> getDeviceList() {
    return this.deviceList;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    if (qName.equalsIgnoreCase("device")) {
      String deviceN = attributes.getValue("deviceID");
      device = new Device();
      device.setDeviceID(Integer.parseInt(deviceN));
    } else if (qName.equalsIgnoreCase("name")) {
      bName = true;
    } else if (qName.equalsIgnoreCase("price")) {
      bPrice = true;
    } else if (qName.equalsIgnoreCase("critical")) {
      bСritical = true;
    } else if (qName.equalsIgnoreCase("origin")) {
      bOrig = true;
    } else if (qName.equalsIgnoreCase("types")) {
      bTypes = true;
    } else if (qName.equalsIgnoreCase("type")) {
      bType = true;
    } else if (qName.equalsIgnoreCase("complection")) {
      bComplection = true;
    } else if (qName.equalsIgnoreCase("interPrice")) {
      bInp = true;
    } else if (qName.equalsIgnoreCase("multimedia")) {
      bMult = true;
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (qName.equalsIgnoreCase("device")) {
      deviceList.add(device);
    }
  }

  public void characters(char ch[], int start, int length) throws SAXException {
    if (bName) {
      device.setName(new String(ch, start, length));
      bName = false;
    } else if (bPrice) {
      device.setPrice(new String(ch, start, length));
      bPrice = false;
    } else if (bСritical) {
      device.setСritical(Boolean.parseBoolean(new String(ch, start, length)));
      bСritical = false;
    } else if (bOrig) {
      String orig = new String(ch, start, length);
      device.setOrigin(orig);
      bOrig = false;
    } else if (bTypes) {
      types = new ArrayList<>();
      bTypes = false;
    } else if (bType) {
      Type type = new Type();
      type.setName(new String(ch, start, length));
      types.add(type);
      bType = false;
    } else if (bComplection) {
      complection = new Complection();
      bComplection = false;
    } else if (bInp) {
      double inp = Double.parseDouble(new String(ch, start, length));
      complection.setInterPrice(inp);
      bInp = false;
    } else if (bMult) {
      boolean mult = Boolean.parseBoolean(new String(ch, start, length));
      complection.setMultimedia(mult);
      device.setComplection(complection);
      device.setTypes(types);
      bMult = false;
    }
  }
}

