package parser.dom;

import java.util.ArrayList;
import java.util.List;
import model.Device;
import model.Complection;
import model.Type;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMDocReader {

  public List<Device> readDoc(Document doc) {
    doc.getDocumentElement().normalize();
    List<Device> devices = new ArrayList<>();

    NodeList nodeList = doc.getElementsByTagName("device");

    for (int i = 0; i < nodeList.getLength(); i++) {
      Device device = new Device();
      Complection complection;
      List<Type> types;

      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;

        device.setDeviceID(Integer.parseInt(element.getAttribute("deviceID")));
        device.setName(element.getElementsByTagName("name").item(0).getTextContent());
        device.setPrice(element.getElementsByTagName("price").item(0).getTextContent());
        device.setСritical(Boolean
            .parseBoolean(element.getElementsByTagName("critical").item(0).getTextContent()));
        device.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());

        types = getTypes(element.getElementsByTagName("types"));
        complection = getComplection(element.getElementsByTagName("complection"));

        device.setComplection(complection);
        device.setTypes(types);
        devices.add(device);
      }
    }
    return devices;
  }

  private List<Type> getTypes(NodeList nodes) {
    List<Type> types = new ArrayList<>();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      NodeList nodeList = element.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element el = (Element) node;
          types.add(new Type(el.getTextContent()));
        }
      }
    }

    return types;
  }

  private Complection getComplection(NodeList nodes) {
    Complection complection = new Complection();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      complection
          .setInterPrice(Double
              .parseDouble(element.getElementsByTagName("interPrice").item(0).getTextContent()));
      complection
          .setMultimedia(Boolean
              .parseBoolean(element.getElementsByTagName("multimedia").item(0).getTextContent()));
    }

    return complection;
  }

}
