package parser;

import comparator.DeviceComparator;
import filechecker.ExtensionChecker;
import model.Device;
import parser.dom.DOMParserUser;
import parser.sax.SAXParserUser;
import parser.stax.StAXReader;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {

  public static void main(String[] args) {
    File xml = new File("src\\main\\resources\\xml\\deviceXML.xml");
    File xsd = new File("src\\main\\resources\\xml\\deviceXSD.xsd");

    if (checkIfXML(xml) && checkIfXSD(xsd)) {
      // DOM
      printList(DOMParserUser.getDeviceList(xml, xsd), "DOM");

      // SAX
      printList(SAXParserUser.parseDevices(xml, xsd), "SAX");

      // StAX
      printList(StAXReader.parseDevices(xml, xsd), "StAX");
    }
  }

  private static boolean checkIfXSD(File xsd) {
    return ExtensionChecker.isXSD(xsd);
  }

  private static boolean checkIfXML(File xml) {
    return ExtensionChecker.isXML(xml);
  }

  private static void printList(List<Device> devices, String parserName) {
    Collections.sort(devices, new DeviceComparator());
    System.out.println("************ " + parserName + " ************ / Sorted by interPrice /");
    for (Device device : devices) {
      System.out.println(device);
      System.out.println("--------------------------------------------------------------");
    }
    System.out.println("--------------------------------------------------------------");
  }

}
