<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: yellow;
                    color: red;
                    text-align:right;
                    }

                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }

                </style>
            </head>

            <body>
                <table class="tfmt">
                    <tr>
                        <th style="width:250px">Name</th>
                        <th style="width:350px">type</th>
                        <th style="width:250px">critical</th>
                        <th style="width:250px">origin</th>
                        <th style="width:250px">types</th>
                        <th style="width:250px">interPrice</th>
                        <th style="width:250px">multimedia</th>

                    </tr>

                    <xsl:for-each select="devices/device">

                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="@deviceID"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="name" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="type" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="critical" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="origin" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="type" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="interPrice" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="multimedia" />
                            </td>

                        </tr>

                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>